import Meteo.RepositoryMeteo
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.entities.ChatId
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class Main {
}

    val repositoryLlum = RepositoryLlum()
    val repositoryMeteo = RepositoryMeteo()
suspend fun main() {
    val llum = HttpClient(CIO) {
        install(ContentNegotiation){
            json()
        }
    }

    val bot = bot {
        token = "5721309762:AAH0aiu2gyv6Hc4gNIf04S6JsOe62RnMysY"
        dispatch {
            command("meteoCity"){
                val msg = args.joinToString(" ")
                GlobalScope.launch {
                    val stringBody = repositoryMeteo.getStringProvincia(msg)
                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "$stringBody")
                }
            }
            command("meteoCities"){
                val msg = args.joinToString(" ")
                GlobalScope.launch {
                    val stringBody = repositoryMeteo.getStringProvincias()
                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "$stringBody")
                }
            }
            command("cheapestDay") {
                GlobalScope.launch {
                    val stringBody = repositoryLlum.getJsonValorCheapest()
                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "$stringBody")
                }
                bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "$")
            }
            command("cheapestDays") {
                GlobalScope.launch {
                    val stringBody = repositoryLlum.getjsonValorAllDay()
                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "$stringBody")
                }
                bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "$")
            }
        }
    }
    bot.startPolling()
}