package Meteo
import java.util.Scanner
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.serialization.kotlinx.json.*

class RepositoryMeteo() {
    val client = HttpClient(CIO) {
        install(ContentNegotiation){
            json()
        }
    }
    suspend fun getStringBody(): String {
        //Devuelve un String de una zona.
        val stringBody: HttpResponse = client.get("https://api.preciodelaluz.org/v1/prices/now?zone=PCB")
        return stringBody.body()
    }

    suspend fun getStringProvincias(): String {
        //Lista de provincias
        val provinceList: HttpResponse = client.get("https://www.el-tiempo.net/api/json/v2/provincias")
        return provinceList.body()
    }

    suspend fun getStringProvincia(provincia: String): String {
        //Devuelve un json de la hora mas barata
        var code = ""
        when (provincia) {
            "Araba" -> code = ("01").toString()
            "Álava" -> code = ("01").toString()
            "Albacete" -> code = ("02").toString()
            "Alacant" -> code = ("03").toString()
            "Alicante" -> code = ("03").toString()
            "Almería" -> code = ("04").toString()
            "Ávila" -> code = ("05").toString()
            "Badajoz" -> code = ("06").toString()
            "Illes Balears" -> code = ("07").toString()
            "Barcelona" -> code = ("08").toString()
            "Burgos" -> code = ("09").toString()
            "Cáceres" -> code = ("10").toString()
            "Cádiz" -> code = ("11").toString()
            "Castelló" -> code = ("12").toString()
            "Castellón" -> code = ("12").toString()
            "Ciudad Real" -> code = ("13").toString()
            "Córdoba" -> code = ("14").toString()
            "A coruña" -> code = ("15").toString()
            "Cuenca" -> code = ("16").toString()
            "Girona" -> code = ("17").toString()
            "Granada" -> code = ("18").toString()
            "Guadalajara" -> code = ("19").toString()
            "Gipuzkoa" -> code = ("20").toString()
            "Huelva" -> code = ("21").toString()
            "Huesca" -> code = ("22").toString()
            "Jaén" -> code = ("23").toString()
            "León" -> code = ("24").toString()
            "Lleida" -> code = ("25").toString()
            "La Rioja" -> code = ("26").toString()
            "Lugo" -> code = ("27").toString()
            "Madrid" -> code = ("28").toString()
            "Málaga" -> code = ("29").toString()
            "Murcia" -> code = ("30").toString()
            "Navarra" -> code = ("31").toString()
            "Ourense" -> code = ("32").toString()
            "Asturias" -> code = ("33").toString()
            "Palencia" -> code = ("34").toString()
            "Las Palmas" -> code = ("35").toString()
            "Pontevedra" -> code = ("36").toString()
            "Salamanca" -> code = ("37").toString()
            "Santa Cruz de Tenerife" -> code = ("38").toString()
            "Cantabria" -> code = ("39").toString()
            "Segovia" -> code = ("40").toString()
            "Sevilla" -> code = ("41").toString()
            "Soria" -> code = ("42").toString()
            "Tarragona" -> code = ("43").toString()
            "Teruel" -> code = ("44").toString()
            "Toledo" -> code = ("45").toString()
            "València" -> code = ("46").toString()
            "Valencia" -> code = ("46").toString()
            "Valladolid" -> code = ("47").toString()
            "Bizkaia" -> code = ("48").toString()
            "Zamora" -> code = ("49").toString()
            "Zaragoza" -> code = ("50").toString()
            "Ceuta" -> code = ("51").toString()
            "Melilla" -> code = ("52").toString()
        }

        val infoProvice: HttpResponse = client.get("https://www.el-tiempo.net/api/json/v2/provincias/$code")
        return infoProvice.body()
    }
}
//Funciones utilizadas para intentar usar un objeto pero no ha llegado a funcionar del todo...
//Lo dejamos aquí por si en un futuro tenemos que utilizarlo para alguna cosa.
suspend fun main() {
    val tiempo = HttpClient(CIO) {
        install(ContentNegotiation){
            json()
        }
    }

    //Devuelve un String de una zona.
    val stringBody: HttpResponse = tiempo.get("https://www.el-tiempo.net/api/json/v2/home")
    val stringResponse: String = stringBody.body()

    //Lista de provincias
    val provinceList: HttpResponse = tiempo.get("https://www.el-tiempo.net/api/json/v2/provincias")
    val proviceRespone: String = provinceList.body()

    //Información geográfica y meteorológica de una provincia
    val scanner_variable = Scanner(System.`in`)
    val provincia = scanner_variable.nextLine()

//    println(provincia)
//    println(proviceRespone)
//    println(stringResponse)




    //Información geográfica y meteorológica de un municipio
    //"02001">Abengibre
    //"02002">Alatoz
    //"02003">Albacete
    //"02004">Albatana
    //"02005">Alborea
    //"02006">Alcadozo
    //"02007">Alcalá del Júcar
    //"02008">Alcaraz
    //"02009">Almansa
    //"02010">Alpera
    //"02011">Ayna
    //"02012">Balazote
    //"02013">Balsa de Ves
    //"02014">El Ballestero
    //"02015">Barrax
    //"02016">Bienservida
    //"02017">Bogarra
    //"02018">Bonete
    //"02019">El Bonillo
    //"02020">Carcelén
    //"02021">Casas de Juan Núñez
    //"02022">Casas de Lázaro
    //"02023">Casas de Ves
    //"02024">Casas-Ibáñez
    //"02025">Caudete
    //"02026">Cenizate
    //"02027">Corral-Rubio
    //"02028">Cotillas
    //"02029">Chinchilla de Monte-Aragón
    //"02030">Elche de la Sierra
    //"02031">Férez
    //"02032">Fuensanta
    //"02033">Fuente-Álamo
    //"02034">Fuentealbilla
    //"02035">La Gineta
    //"02036">Golosalvo
    //"02037">Hellín
    //"02038">La Herrera
    //"02039">Higueruela
    //"02040">Hoya-Gonzalo
    //"02041">Jorquera
    //"02042">Letur
    //"02043">Lezuza
    //"02044">Liétor
    //"02045">Madrigueras
    //"02046">Mahora
    //"02047">Masegoso
    //"02048">Minaya
    //"02049">Molinicos
    //"02050">Montalvos
    //"02051">Montealegre del Castillo
    //"02052">Motilleja
    //"02053">Munera
    //"02054">Navas de Jorquera
    //"02055">Nerpio
    //"02056">Ontur
    //"02057">Ossa de Montiel
    //"02058">Paterna del Madera
    //"02059">Peñascosa
    //"02060">Peñas de San Pedro
    //"02061">Pétrola
    //"02062">Povedilla
    //"02063">Pozohondo
    //"02064">Pozo-Lorente
    //"02065">Pozuelo
    //"02066">La Recueja
    //"02067">Riópar
    //"02068">Robledo
    //"02069">La Roda
    //"02070">Salobre
    //"02071">San Pedro
    //"02072">Socovos
    //"02073">Tarazona de la Mancha
    //"02074">Tobarra
    //"02075">Valdeganga
    //"02076">Vianos
    //"02077">Villa de Ves
    //"02078">Villalgordo del Júcar
    //"02079">Villamalea
    //"02080">Villapalacios
    //"02081">Villarrobledo
    //"02082">Villatoya
    //"02083">Villavaliente
    //"02084">Villaverde de Guadalimar
    //"02085">Viveros
    //"02086">Yeste

//    val codeProv = readln()
//    val id = readln()
//    val infoMunicipi : HttpResponse = tiempo.get("https://www.el-tiempo.net/api/json/v2/provincias/$codeProv/municipios/$id")
//    val infoResponseMunicipi : String = infoMunicipi.body()
}


