import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PrecioLuz(val date: String,
                     val hour: String,
                     @SerialName("is-cheap")
                     val is_cheap: Boolean,
                     @SerialName("is-under-avg")
                     val is_under_avg: Boolean,
                     val market: String,
                     val price: Double,
                     val units: String )
@Serializable
data class Hour(
    @SerialName("00-01")
    val `00-01`: PrecioLuz,
    val `01-02`: PrecioLuz,
    val `02-03`: PrecioLuz,
    val `03-04`: PrecioLuz,
    val `04-05`: PrecioLuz,
    val `05-06`: PrecioLuz,
    val `06-07`: PrecioLuz,
    val `07-08`: PrecioLuz,
    val `08-09`: PrecioLuz,
    val `09-10`: PrecioLuz,
    val `10-11`: PrecioLuz,
    val `11-12`: PrecioLuz,
    val `12-13`: PrecioLuz,
    val `13-14`: PrecioLuz,
    val `14-15`: PrecioLuz,
    val `15-16`: PrecioLuz,
    val `16-17`: PrecioLuz,
    val `17-18`: PrecioLuz,
    val `18-19`: PrecioLuz,
    val `19-20`: PrecioLuz,
    val `20-21`: PrecioLuz,
    val `21-22`: PrecioLuz,
    val `22-23`: PrecioLuz,
    val `23-24`: PrecioLuz
)

class RepositoryLlum() {
    val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json()
        }
    }
    suspend fun getStringBody(): String {
        //Devuelve un String de una zona.
        val stringBody: HttpResponse = client.get("https://api.preciodelaluz.org/v1/prices/now?zone=PCB")
        return stringBody.body()
    }

    suspend fun getjsonValorAllDay(): Hour {
        //Devuelve un Json del dia entero.
        return client.get("https://api.preciodelaluz.org/v1/prices/all?zone=PCB").body()
    }

    suspend fun getJsonValorCheapest(): PrecioLuz {
        //Devuelve un json de la hora mas barata
        return client.get("https://api.preciodelaluz.org/v1/prices/min?zone=PCB").body()
    }
}



